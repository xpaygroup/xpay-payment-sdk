// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "XPayGatewaySDK",
    platforms: [
        .iOS(.v13),
    ],
    products: [
        .library(
            name: "XPayGatewaySDK",
            targets: ["XPayGatewaySDK", "Wrapper"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/centrifugal/centrifuge-swift", from: "0.5.5")
    ],
    targets: [
        .target(
            name: "Wrapper",
            dependencies: [
                .product(name: "SwiftCentrifuge", package: "centrifuge-swift")
            ],
            path: "Sources/Wrapper"
        ),
        .binaryTarget(
            name: "XPayGatewaySDK",
            path: "XPayGatewaySDK.xcframework"
        )
    ]
)
