- [Getting started](#getting-started)
  * [Integration](#integration)
    + [Swift Package Manager](#swift-package-manager)
    + [CocoaPods](#cocoapods)
    + [Dynamic Framework Installation](#dynamic-framework-installation)
- [Appearance](#appearance)
- [Localization](#localization)
  * [Introduction](#introduction)

## Getting started

### Integration

#### Swift Package Manager

This is the recommended approach of integration the SDK with your app. You can follow Apple's guide for [adding a package dependency to your app](https://developer.apple.com/documentation/xcode/adding_package_dependencies_to_your_app "adding a package dependency to your app") for a thorough walkthrough.

#### CocoaPods

1. Add the following line to your podfile:

    ```ruby
    pod 'XPayGatewaySDK', :git => 'https://gitlab.com/xpaygroup/xpay-payment-sdk.git'
```

2. Run `pod install`.

3. Import the SDK module:
    ```swift
    import XPayGatewaySDK
```

#### Dynamic Framework Installation

1. Download `XPayGatewaySDK.xcframework` from repository

2. Drag the `XPayGatewaySDK.xcframework` into your application project.

3. Install [SwiftCentrifuge](https://github.com/centrifugal/centrifuge-swift/blob/master/Package.swift "SwiftCentrifuge") dependency

4. Add `XPayGatewaySDK.xcframework` target as a dependency:
    - Navigate to Build Phases > Target Dependencies.
    - Add `XPayGatewaySDK.xcframework`.

5. Make sure that the framework is copied into the bundle:
    - Navigate to Build Phases > New Copy Files Phase.
    - From the Destination dropdown, select Frameworks.
    - Add `XPayGatewaySDK.xcframework`.

6. Import the SDK module:
    ```swift
    import XPayGatewaySDK
```

###Example

The `presentPaymentSheet` function is used to present the payment sheet.

#### Parameters
- `configuration` (`XPayGateway.Configuration`): The configuration object that provides the necessary details for the payment sheet, such as API credentials, user preferences.
- `paymentInfo` (`XPayGateway.PaymentInfo`): The payment information required for processing the payment, including the amount, and payment method details.
- `startingScreen` (`XPayGateway.StartingScreen`): The starting screen to display when presenting the payment sheet.
- `viewController` (`UIViewController`): The view controller from which to present the payment sheet.
- `presentationStyle` (`UIModalPresentationStyle`, optional): The modal presentation style for the payment sheet. Default value is `.automatic`.
- `appearance` (XPayGateway.Appearance, optional): The appearance configuration for customizing the payment sheet's appearance. Default value is `nil`.
- `overrideTranslation` (`(TranslationOverrideUtil) -> Void`, optional): A closure that allows overriding translations for localization purposes. It takes a `TranslationOverrideUtil` instance as a parameter. Default value is `nil`.
- `isDebug` (`Bool`, optional): A boolean value indicating whether the payment sheet is in debug mode. Default value is `false`.

## Usage
```swift
    XPayGateway.presentPaymentSheet(
        with: configuration,
        paymentInfo: paymentInfo,
        startingScreen: startingScreen,
        in: viewController,
        presentationStyle: presentationStyle,
        appearance: appearance,
        overrideTranslation: overrideTranslation,
        isDebug: isDebug
    )
```

## Appearance

The `Appearance` struct is a container that holds appearance-related properties for various parts of an framework, including navigation bar, checkout, payment, receipt, and failure screens.
```swift
    let appearance = XPayGateway.Appearance(
      navigationBarAppearance: .init(
        titleTextAttributes: [
            .foregroundColor: UIColor.green,
            .font: UIFont.systemFont(ofSize: 16.0, weight: .regular)
        ],
        backIndicatorImage: **nil**,
        tintColor: UIColor.red
        )
    )
```

[Follow the link to find out more](Documentation/Appearance/README.md)

## Localization

### Introduction
The XPayGateway provides a way to override translations for localization purposes. This allows you to customize the text displayed in the payment sheet according to your application's specific language or requirements. The `presentPaymentSheet` function accepts an optional `overrideTranslation` parameter, which allows you to override translations using a closure that takes a `TranslationOverrideUtil` instance as a parameter.

1. Define the `overrideTranslation` closure:
```swift
    let overrideTranslation: (TranslationOverrideUtil) -> Void = { translationUtil in
        // Perform translation overrides here
    }
```

2. Within the closure, you have access to a `TranslationOverrideUtil` instance named `translationUtil`, which provides methods to override translations.

3. Use the `override` method of the `translationUtil` instance to override translations. This method takes in a key `LocalizableKey`, language `XPayGateway.Configuration.Language`, and value `String` parameters.

4. Example usage of overrideTranslation closure:
```swift
    let overrideTranslation: (TranslationOverrideUtil) -> Void = { translationUtil in
        translationUtil.override(with: .orderTitle, for: .en, with: "Hello")
        translationUtil.override(with: .orderAmount, for: .en, with: "World")
        // Add more translation overrides as needed
    }
```

5. Pass `overrideTranslation` clouser to `presentPaymentSheet` function

[Follow the link to find out more localization keys](Documentation/Localization/README.md)


