// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.2 (swiftlang-5.7.2.135.5 clang-1400.0.29.51)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name XPayGatewaySDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import AudioToolbox
import Combine
import CommonCrypto
import CommonCrypto.CommonHMAC
import CoreImage
import CryptoKit
import Foundation
import PassKit
import Swift
import SwiftCentrifuge
import UIKit
import Vision
import WebKit
import _Concurrency
import _StringProcessing
public enum LocalizableKey : Swift.String {
  case orderTitle
  case orderPay
  case orderTotalToPay
  case orderAmount
  case orderCommission
  case orderExpandableCardTitle
  case orderExpandablePaymentInfo
  case orderExpandablePayee
  case orderExpandableId
  case orderExpandableAccount
  case orderExpandableOperationId
  case paymentTitle
  case paymentAmount
  case paymentMethodTitle
  case paymentMethodApple
  case paymentMethodCard
  case paymentMethodMyCards
  case paymentSaveCard
  case paymentSaveDialogCaption
  case paymentSaveDialogOk
  case paymentRemove
  case paymentRemoveCardDialogTitle
  case paymentRemoveCardDialogCaption
  case paymentRemoveCardDialogRemove
  case paymentRemoveCardDialogCancel
  case paymentPay
  case scanTitle
  case paymentSuccessful
  case paymentReceiptByEmail
  case paymentReceiptSended
  case paymentEmailIncorrect
  case paymentBackToHomeScreen
  case paymentSend
  case paymentOperationError
  case paymentRequestId
  case paymentReason
  case paymentTryAnotherCard
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension XPayGatewaySDK.XPayGateway {
  public struct Configuration {
    public enum Language : Swift.String, Swift.CaseIterable {
      case en
      case uk
      public init?(rawValue: Swift.String)
      public typealias AllCases = [XPayGatewaySDK.XPayGateway.Configuration.Language]
      public typealias RawValue = Swift.String
      public static var allCases: [XPayGatewaySDK.XPayGateway.Configuration.Language] {
        get
      }
      public var rawValue: Swift.String {
        get
      }
    }
    public let userPhone: Swift.String
    public let partnerToken: Swift.String
    public let signatureBackendURL: Foundation.URL
    public let language: XPayGatewaySDK.XPayGateway.Configuration.Language
    public let merchantIdentifier: Swift.String
    public init(userPhone: Swift.String, partnerToken: Swift.String, signatureBackendURL: Foundation.URL, merchantIdentifier: Swift.String, language: XPayGatewaySDK.XPayGateway.Configuration.Language = .en)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct NavigationBarAppereance {
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]?, backIndicatorImage: UIKit.UIImage?, tintColor: UIKit.UIColor?)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct ReceiptAppearance {
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, subtitleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, backgroundColor: UIKit.UIColor? = nil, textFieldAppearance: XPayGatewaySDK.XPayGateway.Appearance.ReceiptAppearance.TextFieldAppearance? = nil, buttonAppearance: XPayGatewaySDK.XPayGateway.Appearance.ButtonAppearance? = nil, image: UIKit.UIImage? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance.ReceiptAppearance {
  public struct TextFieldAppearance {
    public init(topPlaceholderTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, textAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, placeholderTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, borderColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct ButtonAppearance {
    public init(backgroundColor: UIKit.UIColor, disabledBackgroundColor: UIKit.UIColor, titleColor: UIKit.UIColor, titleFont: UIKit.UIFont)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct CheckoutAppearance {
    public struct PaymentItemAppearance {
      public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, subtitleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, priceTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, backgroundColor: UIKit.UIColor? = nil, checkboxTintColor: UIKit.UIColor? = nil)
    }
    public struct PaymentDetailItemAppearance {
      public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]?, priceTextAttributes: [Foundation.NSAttributedString.Key : Any]?, currencyTextAttributes: [Foundation.NSAttributedString.Key : Any]?)
    }
    public struct PaymentInfoAppearance {
      public struct PaymentInfoItem {
        public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, subtitleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil)
      }
      public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, subtitleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, paymentInfoItemAppearance: XPayGatewaySDK.XPayGateway.Appearance.CheckoutAppearance.PaymentInfoAppearance.PaymentInfoItem?, backgroundColor: UIKit.UIColor?, arrowColor: UIKit.UIColor?)
    }
    public init(paymentItemAppearance: XPayGatewaySDK.XPayGateway.Appearance.CheckoutAppearance.PaymentItemAppearance? = nil, paymentDetailItemAppearance: XPayGatewaySDK.XPayGateway.Appearance.CheckoutAppearance.PaymentDetailItemAppearance? = nil, paymentInfoAppearance: XPayGatewaySDK.XPayGateway.Appearance.CheckoutAppearance.PaymentInfoAppearance? = nil, backgroundColor: UIKit.UIColor? = nil, buttonAppearance: XPayGatewaySDK.XPayGateway.Appearance.ButtonAppearance? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct PaymentAppearance {
    public init(amountAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.AmountAppearance? = nil, paymentMethodSelectorAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.PaymentMethodSelectorAppearance? = nil, newCardAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.NewCardAppearance? = nil, cardItemAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.CardItemAppearance? = nil, buttonAppearance: XPayGatewaySDK.XPayGateway.Appearance.ButtonAppearance? = nil, backgroundColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance {
  public struct AmountAppearance {
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, priceTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, borderColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance {
  public struct PaymentMethodSelectorAppearance {
    public struct PaymentMethodAppearance {
      public init(backgroundColor: UIKit.UIColor, selectedBackgroundColor: UIKit.UIColor, titleTextAttributes: [Foundation.NSAttributedString.Key : Any], selectedTitleTextAttributes: [Foundation.NSAttributedString.Key : Any], image: UIKit.UIImage?, selectedImage: UIKit.UIImage?)
    }
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, applePayPaymentMethodAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.PaymentMethodSelectorAppearance.PaymentMethodAppearance? = nil, newCardPaymentMethodAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.PaymentMethodSelectorAppearance.PaymentMethodAppearance? = nil, savedCardPaymentMethodAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance.PaymentMethodSelectorAppearance.PaymentMethodAppearance? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance {
  public struct NewCardAppearance {
    public init(textColor: UIKit.UIColor? = nil, placeholderColor: UIKit.UIColor? = nil, scanCardColor: UIKit.UIColor? = nil, backgroundImage: UIKit.UIImage? = nil, toggleTintColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance {
  public struct CardItemAppearance {
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, backgroundColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway {
  public struct Appearance {
    public init(navigationBarAppearance: XPayGatewaySDK.XPayGateway.Appearance.NavigationBarAppereance? = nil, checkoutAppearance: XPayGatewaySDK.XPayGateway.Appearance.CheckoutAppearance? = nil, paymentAppearance: XPayGatewaySDK.XPayGateway.Appearance.PaymentAppearance? = nil, receiptAppearance: XPayGatewaySDK.XPayGateway.Appearance.ReceiptAppearance? = nil, failureAppearance: XPayGatewaySDK.XPayGateway.Appearance.FailureAppearance? = nil)
  }
}
extension XPayGatewaySDK.XPayGateway {
  public struct PaymentInfo {
    public struct PaymentItem {
      public let title: Swift.String
      public let subtitle: Swift.String
      public let price: Swift.Int
      public init(title: Swift.String, subtitle: Swift.String, price: Swift.Int)
    }
    public let paymentItems: [XPayGatewaySDK.XPayGateway.PaymentInfo.PaymentItem]
    public let totalPrice: Swift.Int
    public let paymentPrice: Swift.Int
    public let fee: Swift.Int
    public init(paymentItems: [XPayGatewaySDK.XPayGateway.PaymentInfo.PaymentItem], fee: Swift.Int)
  }
}
public struct XPayGateway {
  public enum StartingScreen {
    case checkout
    case payment
    public static func == (a: XPayGatewaySDK.XPayGateway.StartingScreen, b: XPayGatewaySDK.XPayGateway.StartingScreen) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @_hasMissingDesignatedInitializers public class TranslationOverrideUtil {
    public func override(with key: XPayGatewaySDK.LocalizableKey, for language: XPayGatewaySDK.XPayGateway.Configuration.Language, with value: Swift.String)
    @objc deinit
  }
  public static func presentPaymentSheet(with configuration: XPayGatewaySDK.XPayGateway.Configuration, paymentInfo: XPayGatewaySDK.XPayGateway.PaymentInfo, startingScreen: XPayGatewaySDK.XPayGateway.StartingScreen, in viewController: UIKit.UIViewController, presentationStyle: UIKit.UIModalPresentationStyle = .automatic, appearance: XPayGatewaySDK.XPayGateway.Appearance? = nil, overrideTranslation: ((XPayGatewaySDK.XPayGateway.TranslationOverrideUtil) -> Swift.Void)? = nil, isDebug: Swift.Bool = false)
}
extension XPayGatewaySDK.XPayGateway.Appearance {
  public struct FailureAppearance {
    public init(titleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, itemTitleTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, itemDescriptionTextAttributes: [Foundation.NSAttributedString.Key : Any]? = nil, buttonAppearance: XPayGatewaySDK.XPayGateway.Appearance.ButtonAppearance? = nil, backgroundColor: UIKit.UIColor? = nil, borderColor: UIKit.UIColor? = nil)
  }
}
extension XPayGatewaySDK.LocalizableKey : Swift.Equatable {}
extension XPayGatewaySDK.LocalizableKey : Swift.Hashable {}
extension XPayGatewaySDK.LocalizableKey : Swift.RawRepresentable {}
extension XPayGatewaySDK.XPayGateway.Configuration.Language : Swift.Equatable {}
extension XPayGatewaySDK.XPayGateway.Configuration.Language : Swift.Hashable {}
extension XPayGatewaySDK.XPayGateway.Configuration.Language : Swift.RawRepresentable {}
extension XPayGatewaySDK.XPayGateway.StartingScreen : Swift.Equatable {}
extension XPayGatewaySDK.XPayGateway.StartingScreen : Swift.Hashable {}
