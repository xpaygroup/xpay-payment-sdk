Pod::Spec.new do |s|
  s.name          = 'XPayGatewaySDK'
  s.version       = '1.0'
  s.summary       = 'PSP (Payment Service Provider)'
  s.homepage      = 'https://www.xpay.com.ua'
  s.license       = { :type => 'MIT' }
  s.author        = { 'XPayGatewaySDK' => 'info@xpay.com.ua' }
  s.source     = { :git => 'https://gitlab.com/xpaygroup/xpay-payment-sdk.git' }
  s.swift_version = '5.7.2'
  s.ios.deployment_target = '13.0'

  s.dependency 'SwiftCentrifuge', '~> 0.5.5'

  s.vendored_frameworks = 'XPayGatewaySDK.xcframework'
end
