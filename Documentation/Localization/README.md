# Localizable Keys

| Key | English | Ukrainian |
| --- | --- | --- |
| **Order Screen** | |
| `orderTitle` | Placing an order | Розміщення замовлення |
| `orderPay` | Pay | Оплатити |
| `orderTotalToPay` | Amount to be paid | Сума до сплати |
| `orderAmount` | Payment amount | Сума платежу |
| `orderCommission` | Commission | Комісія |
| `orderExpandableCardTitle` | xpay.com.ua | xpay.com.ua |
| `orderExpandablePaymentInfo` | Payment information | Інформація про платіж |
| `orderExpandablePayee` | Recipient | Одержувач |
| `orderExpandableId` | Recipient code | Код одержувача |
| `orderExpandableAccount` | Account | Рахунок |
| `orderExpandableOperationId` | Operation ID | ID операції |
| **Payment Screen** | |
| `paymentTitle` | Payment | Оплата |
| `paymentAmount` | Amount to be paid | Сума до сплати |
| `paymentMethodTitle` | Choose a payment method | Оберіть спосіб оплати |
| `paymentMethodApple` | Apple Pay | Apple Pay |
| `paymentMethodCard` | Card | Карта |
| `paymentMethodMyCards` | My cards | Мої карти |
| `paymentSaveCard` | Remember the card | Запам'ятати карту |
| `paymentSaveDialogCaption` | Card data will be encrypted and saved. This allows you to make subsequent payments in one click | Дані картки будуть зашифровані й збережені. Це дозволить здійснювати наступні платежі одним кліком |
| `paymentSaveDialogOk` | OK | OK |
| `paymentRemove` | Delete | Видалити |
| `paymentRemoveCardDialogTitle` | Deleting a card | Видалення картки |
| `paymentRemoveCardDialogCaption` | Do you really want to delete the card? | Дійсно бажаєте видалити картку? |
| `paymentRemoveCardDialogRemove` | Yes, delete | Так, видалити |
| `paymentRemoveCardDialogCancel` | Cancel | Скасувати |
| `paymentPay` | Pay | Оплатити |
| **Successful Screen** | |
| `paymentSuccessful` | The payment was made successfully | Платіж успішно здійснено |
| `paymentReceiptByEmail` | Do you want to receive a receipt by email? | Бажаєте отримати квитанцію електронною поштою? |
| `paymentReceiptSended` | Receipt has been sent | Квитанція відправлена |
| `paymentEmailIncorrect` | Invalid email format | Неправильний формат електронної пошти |
| `paymentBackToHomeScreen` | Return to the main page | Повернутися на головну сторінку |
| `paymentSend` | Send | Відправити
| **Unsuccessful Screen** | |
| `paymentOperationError` | Operation error | Помилка операції |
| `paymentRequestId` | REQUEST ID: | ID ЗАПИТУ: |
| `paymentReason` | REASON: | ПРИЧИНА: |
| `paymentTryAnotherCard` | Try another card | Спробуйте іншу картку |
