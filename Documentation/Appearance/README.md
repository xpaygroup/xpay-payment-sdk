- [Navigation Bar](#navigation-bar)
  * [ButtonAppearance](#buttonappearance)
    + [Properties](#properties)
- [Checkout Screen](#checkout-screen)
    + [CheckoutAppearance](#checkoutappearance)
    + [PaymentItemAppearance](#paymentitemappearance)
    + [PaymentDetailItemAppearance](#paymentdetailitemappearance)
    + [PaymentInfoAppearance](#paymentinfoappearance)
      - [PaymentInfoItem](#paymentinfoitem)
- [Payment Screen](#payment-screen)
    + [AmountAppearance](#amountappearance)
    + [PaymentMethodAppearance](#paymentmethodappearance)
    + [PaymentMethodSelectorAppearance](#paymentmethodselectorappearance)
    + [NewCardAppearance](#newcardappearance)
    + [CardItemAppearance](#carditemappearance)
- [Receipt Screen](#receipt-screen)
    + [ReceiptAppearance](#receiptappearance)
- [Failure Screen](#failure-screen)
    + [FailureAppearance](#failureappearance)
    + [Properties:](#properties-)

# Navigation Bar

-   `titleTextAttributes: [NSAttributedString.Key : Any]?`: A dictionary that specifies the text attributes for the navigation bar title, such as font, color, and text shadow.
```swift
    [
        .foregroundColor: UIColor(hex: "#000000"),
        .font: UIFont.systemFont(ofSize: 16.0, weight: .regular)
    ]
```
-   `backIndicatorImage: UIImage?`: An optional image that appears as the back button in the navigation bar.

```swift
UIImage(named: "ic_back")
```
-   `tintColor: UIColor?`: The tint color to apply to the navigation bar background and buttons.

```swift
UIColor(hex: "#000000")
```

## ButtonAppearance

The `ButtonAppearance` struct describes the visual appearance of a button, such as its background color, title color, font, etc.

### Properties

-   `backgroundColor: UIColor`: The background color of the button.

```swift
UIColor(hex: "#2751E9")
```

-   `disabledBackgroundColor: UIColor`: The background color of the button when it is disabled.

```swift
UIColor(hex: "#E1E6FA")
```

-   `titleColor: UIColor`: The color of the button's title text.

```swift
UIColor(hex: "#FFFFFF")
```

-   `titleFont: UIFont`: The font of the button's title text.

```swift
UIFont.systemFont(ofSize: 12.0, weight: .medium)
```

# Checkout Screen

### CheckoutAppearance

This struct defines the overall appearance of the checkout page. It has the following properties:

-   `paymentItemAppearance: PaymentItemAppearance`: an instance of [PaymentItemAppearance](#paymentitemappearance) that defines the appearance of the payment item.

-   `paymentDetailItemAppearance: PaymentDetailItemAppearance`: an instance of [PaymentDetailItemAppearance](#paymentdetailitemappearance) that defines the appearance of the payment detail item.

-   `paymentInfoAppearance: PaymentInfoAppearance`: an instance of [PaymentInfoAppearance](#paymentinfoappearance) that defines the appearance of the payment info.

-   `backgroundColor: UIColor`: the background color of the checkout page.

```swift
UIColor(hex: "#FFFFFF")
```

-   `buttonAppearance: ButtonAppearance`: an instance of [ButtonAppearance](#buttonappearance) that defines the appearance of the checkout button.

The `CheckoutAppearance` struct defines the appearance of the checkout page. It has four nested structs that define the appearance of different parts of the checkout page. These nested structs are:

### PaymentItemAppearance

This struct defines the appearance of the payment item on the checkout page. It has the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment item title.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `subtitleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment item subtitle.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#67768A")
    ]
```

-   `priceTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment item price.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#2751E9")
    ]
```

-   `backgroundColor: UIColor`: the background color of the payment item.

```swift
UIColor(hex: "#FAFBFE")
```

-   `checkboxTintColor: UIColor`: the color of the checkbox for the payment item.

```swift
UIColor(hex: "#2751E9")
```

### PaymentDetailItemAppearance

This struct defines the appearance of the payment detail item on the checkout page. It has the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment detail item title.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#27365B")
    ]
```

-   `priceTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment detail item price.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .bold),
        .foregroundColor: UIColor(hex: "#27365B")
    ]
```

-   `currencyTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment detail item currency.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#27365B")
    ]
```

### PaymentInfoAppearance

This struct defines the appearance of the payment info on the checkout page. It has the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment info title.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#27365B")
    ]
```

-   `subtitleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment info subtitle.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#27365B")
    ]
```

-   `backgroundColor: UIColor`: the background color of the payment info.

```swift
UIColor(hex: "#F8F8FA")
```

-   `arrowColor: UIColor`: the color of the arrow for the payment info.

```swift
UIColor(hex: "#465A8B")
```

-   `paymentInfoItemAppearance: PaymentInfoItem`: an instance of [PaymentInfoItem](#paymentinfoitem) that defines the appearance of the payment info item.

#### PaymentInfoItem

This struct defines the appearance of the payment info item on the checkout page. It has the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment info item title.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .medium),
        .foregroundColor: UIColor(hex: "#67768A")
    ]
```

-   `subtitleTextAttributes: [NSAttributedString.Key : Any]`: a dictionary of text attributes for the payment info item subtitle.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .medium),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

# Payment Screen

###PaymentAppearance

The `PaymentAppearance` structure defines the appearance of the payment screen in the XPayGatewaySDK.

The structure contains the following properties:

-   `amountAppearance: AmountAppearance`: An instance of the [AmountAppearance](#amountappearance) structure that defines the appearance of the amount label in the payment screen.

-   `paymentMethodSelectorAppearance: PaymentMethodSelectorAppearance`: An instance of the [PaymentMethodSelectorAppearance](#paymentmethodselectorappearance) structure that defines the appearance of the payment method selector in the payment screen.

-   `newCardAppearance: NewCardAppearance`: An instance of the [NewCardAppearance](#newcardappearance) structure that defines the appearance of the new card form in the payment screen.

-   `cardItemAppearance: CardItemAppearance`: An instance of the [CardItemAppearance](#carditemappearance) structure that defines the appearance of the saved card items in the payment screen.

-   `buttonAppearance: ButtonAppearance`: An instance of the [ButtonAppearance](#buttonappearance) structure that defines the appearance of the checkout button in the payment screen.

-   `backgroundColor: UIColor`: The background color of the payment screen.

```swift
UIColor(hex: "#FFFFFF")
```

### AmountAppearance

The `AmountAppearance` structure defines the appearance of the amount label in the payment screen.

The structure contains the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: The text attributes of the title label in the amount section.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `priceTextAttributes: [NSAttributedString.Key : Any]`: The text attributes of the price label in the amount section.

```swift
    [
        .font: UIFont.systemFont(ofSize: 24.0, weight: .bold),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `borderColor: [NSAttributedString.Key : Any]`: The color of the border in the amount section.

```swift
UIColor(hex: "#F0F3FC")
```


### PaymentMethodAppearance

The `PaymentMethodAppearance` structure defines the appearance of a payment method button in the payment method selector section.

The structure contains the following properties:

-   `backgroundColor`: The background color of the payment method button.
-   `selectedBackgroundColor`: The background color of the payment method button when selected.
-   `titleTextAttributes`: The text attributes of the title label in the payment method button.
-   `selectedTitleTextAttributes`: The text attributes of the title label in the payment method button when selected.
-   `image`: The image of the payment method button.
-   `selectedImage`: The image of the payment method button when selected.


### PaymentMethodSelectorAppearance

The `PaymentMethodSelectorAppearance` structure defines the appearance of the payment method selector in the payment screen.

The structure contains the following properties:

-   `titleTextAttributes: [NSAttributedString.Key : Any]`: The text attributes of the title label in the payment method selector section.

```swift
    [
        .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `applePayPaymentMethodAppearance: PaymentMethodAppearance`: An instance of the [PaymentMethodAppearance](#paymentmethodappearance) structure that defines the appearance of the Apple Pay payment method button in the payment method selector section.

```swift
    PaymentMethodAppearance(
        backgroundColor: UIColor(hex: "#FFFFFF"),
        selectedBackgroundColor: UIColor(hex: "#000000"),
        titleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .regular),
                .foregroundColor: UIColor(hex: "#111111")
        ],
        selectedTitleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .bold),
                .foregroundColor: UIColor(hex: "#FFFFFF")
        ],
        image: UIImage(named: "ic_apple_pay_inactive"),
        selectedImage: UIImage(named: "ic_apple_pay_active")
    )
```

-   `newCardPaymentMethodAppearance: PaymentMethodAppearance`: An instance of the [PaymentMethodAppearance](#paymentmethodappearance) structure that defines the appearance of the new card payment method button in the payment method selector section.

```swift
    PaymentMethodAppearance(
        backgroundColor: UIColor(hex: "#FFFFFF"),
        selectedBackgroundColor: UIColor(hex: "#88B1FF"),
        titleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .regular),
                .foregroundColor: UIColor(hex: "#111111")
        ],
        selectedTitleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .bold),
                .foregroundColor: UIColor(hex: "#111111")
        ],
        image: UIImage(named: "ic_card_inactive"),
        selectedImage: UIImage(named: "ic_card_active")
    )
```

-   `savedCardPaymentMethodAppearance: PaymentMethodAppearance`: An instance of the [PaymentMethodAppearance](#paymentmethodappearance) structure that defines the appearance of the saved card payment method button in the payment method selector section.

```swift
    PaymentMethodAppearance(
        backgroundColor: UIColor(hex: "#FFFFFF"),
        selectedBackgroundColor: UIColor(hex: "#FFC3C3"),
        titleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .regular),
                .foregroundColor: UIColor(hex: "#111111")
        ],
        selectedTitleTextAttributes: [
                .font: UIFont.systemFont(ofSize: 10.0, weight: .bold),
                .foregroundColor: UIColor(hex: "#111111")
        ],
        image: UIImage(named: "ic_my_card_inactive"),
        selectedImage: UIImage(named: "ic_my_card_active")
    )
```

### NewCardAppearance

This struct defines the appearance of the new card form in the payment screen. It contains the following properties:

-   `textColor`: `UIColor` - Text color of the new card form

```swift
UIColor(hex: "#F1F5FC")
```

-   `placeholderColor`: `UIColor` - Color of the placeholder text in the new card form

```swift
UIColor(hex: "#F1F5FC").withAlphaComponent(0.5)
```

-   `scanCardColor`: `UIColor` - Color of the scan card button in the new card form

```swift
UIColor(hex: "#FFFFFF")
```

-   `backgroundImage`: `UIImage?` - Background image of the new card form

```swift
UIImage(named: "ic_card_background")
```

-   `toggleTintColor`: `UIColor` - Tint color of the toggle switch in the new card form

```swift
UIColor(hex: "#2751E9")
```


### CardItemAppearance

This struct defines the appearance of a saved card item in the payment screen. It contains the following properties:

-   `titleTextAttributes`: `[NSAttributedString.Key : Any]` - Text attributes for the title of the saved card item

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `backgroundColor`: `UIColor` - Background color of the saved card item view

```swift
UIColor(hex: "#FFFFFF")
```

# Receipt Screen

### ReceiptAppearance 
The `ReceiptAppearance` struct is used to define the appearance of a receipt. It has six properties:

-   `titleTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary representing the appearance of the title text.

```swift
    [
        .font: UIFont.systemFont(ofSize: 18.0, weight: .heavy),
        .foregroundColor: UIColor(hex: "#000000")
    ]
```

-   `subtitleTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary representing the appearance of the subtitle text.

```swift
    [
        .font: UIFont.systemFont(ofSize: 16.0, weight: .regular),
        .foregroundColor: UIColor(hex: "#FFFFFF")
    ]
```

-   `backgroundColor`: `UIColor` Representing the background color of the receipt.

```swift
UIColor(hex: "#FFFFFF")
```

-   `textFieldAppearance`: `TextFieldAppearance` A struct that defines the appearance of the text field in the receipt. It has four properties:

    -   `topPlaceholderTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary representing the appearance of the top placeholder text in the text field.
    
        ```swift
    [
            .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
            .foregroundColor: UIColor(hex: "#67768A")
    ]
```
    
    -   `textAttributes`: `[NSAttributedString.Key : Any]` A dictionary representing the appearance of the text in the text field.
    
        ```swift
    [
            .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
            .foregroundColor: UIColor(hex: "#1E1F22")
    ]
```
    
    -   `placeholderTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary representing the appearance of the placeholder text in the text field.
    
        ```swift
    [
            .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
            .foregroundColor: UIColor(hex: "#E0E6F2")
    ]
```
    
    -   `borderColor`: `UIColor` Representing the border color of the text field.
    
        ```swift
UIColor(hex: "#E5E4E9")
```
    
-   `buttonAppearance`: `ButtonAppearance` A struct that defines the appearance of the buttons in the receipt. This struct is not defined in the code snippet, so it is assumed that it is defined elsewhere.

-   `image`: `UIImage` Representing the image to be displayed in the receipt.

```swift
UIImage(named: "ic_success")
```

# Failure Screen

### FailureAppearance


The `FailureAppearance` struct represents the appearance of a failure screen. It contains properties that define the appearance of various UI elements in the failure view.

### Properties:

-   `titleTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary of attributes to apply to the title text in the failure view.

```swift
    [
        .foregroundColor: UIColor(hex: "#F82929"),
        .font: UIFont.systemFont(ofSize: 18.0, weight: .heavy)
    ]
```

-   `itemTitleTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary of attributes to apply to the title text of each item in the failure view.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .semibold),
        .foregroundColor: UIColor(hex: "#465A8B").withAlphaComponent(0.5)
    ]
```

-   `itemDescriptionTextAttributes`: `[NSAttributedString.Key : Any]` A dictionary of attributes to apply to the description text of each item in the failure view.

```swift
    [
        .font: UIFont.systemFont(ofSize: 12.0, weight: .medium),
        .foregroundColor: UIColor(hex: "465A8B")
    ]
```

-   `buttonAppearance`: `ButtonAppearance` An instance of the [ButtonAppearance](#buttonappearance) struct that defines the appearance of the button in the failure view.

-   `backgroundColor`: `UIColor` The background color of the failure view.

    ```swift
UIColor(hex: "#FFFFFF")
```

-   `borderColor`: `UIColor` The border color of the failure view.

    ```swift
UIColor(hex: "#F6F7FB")
```
