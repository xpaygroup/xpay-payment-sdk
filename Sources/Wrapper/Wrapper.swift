// Wrapper Target Explanation:
//
// The Wrapper target serves as a bridge between XPayGatewaySDK and SwiftCentrifuge.
// It allows us to include the source code of SwiftCentrifuge within the XPayGatewaySDK package.
//
// Why use a Wrapper?
// XPayGatewaySDK has a dependency on the external package SwiftCentrifuge, which is not directly
// supported by Swift Package Manager's binaryTarget. Therefore, we create a Wrapper target to
// include the source code of SwiftCentrifuge within our package structure.
//
// How does it work?
// The Wrapper target includes the source code of SwiftCentrifuge and specifies it as a dependency.
// It allows us to import and utilize the SwiftCentrifuge module within the XPayGatewaySDK package.
// The Wrapper target can then provide any necessary adaptations or interfaces for integrating
// SwiftCentrifuge into the XPayGatewaySDK package.
//
// Including the dependency as a source-based target allows for easier management and customization
// of SwiftCentrifuge within the XPayGatewaySDK package, although it requires including the source
// code of SwiftCentrifuge in the package itself.
//
// By including the Wrapper target in the XPayGatewaySDK product, we ensure that the consumer of
// XPayGatewaySDK gets both the core functionality of XPayGatewaySDK and the necessary components
// from SwiftCentrifuge, managed seamlessly within the package.
